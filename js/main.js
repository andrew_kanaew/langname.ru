"use strict";

var nameList = document.querySelectorAll(".name-list__column");
var nameListMore1 = document.querySelectorAll(".button_more")[0];
var nameListMore2 = document.querySelectorAll(".button_more")[1];
var nameListMore = document.querySelectorAll(".button_more")[2];
var nameListIsOpen = false;

if (nameList.length != 0) {
	nameListMore.addEventListener("click", function () {
		if(!nameListIsOpen) {
			nameList[0].style.height = "auto";
			nameList[1].style.height = "auto";
			nameListMore.innerHTML = "Скрыть";
			nameListIsOpen = true;
		} else {
			nameList[0].style.height = "345px";
			nameList[1].style.height = "345px";
			nameListMore.innerHTML = "Показать все имена";
			nameListIsOpen = false;
		}
	});

	nameListMore1.addEventListener("click", function () {
		if(!nameListIsOpen) {
			nameList[0].style.height = "auto";
			nameListMore1.innerHTML = "Скрыть";
			nameListIsOpen = true;
		} else {
			nameList[0].style.height = "345px";
			nameListMore1.innerHTML = "Показать все имена";
			nameListIsOpen = false;
		}
	});

	nameListMore2.addEventListener("click", function () {
		if(!nameListIsOpen) {
			nameList[1].style.height = "auto";
			nameListMore2.innerHTML = "Скрыть";
			nameListIsOpen = true;
		} else {
			nameList[1].style.height = "345px";
			nameListMore2.innerHTML = "Показать все имена";
			nameListIsOpen = false;
		}
	});
}

$(".section__name-compatibility-list a").click(function(event) {
	event.preventDefault();
	console.log($(this).text());
	var female = $(this).text().split(' & ')[0]
	var male = $(this).text().split(' & ')[1]
	$("#female").val(female); 
	$("#male").val(male); 

});

$("#name-compatibility").submit(function (event) {
	event.preventDefault();
	$(".name-compatibility-description_is_active").removeClass('name-compatibility-description_is_active');
})

$(".date-filter .button").click(function (event) {
	$(".date-filter_is_active").removeClass("date-filter_is_active");
	$(".tabs__item_is_active").removeClass("tabs__item_is_active");
	var month = $(this).data("month");
	$(this).addClass("date-filter_is_active");
	$("#" + month).addClass("tabs__item_is_active");
});

$(".calendar-name-toggler").click(function (event) {
	$(this).toggleClass("calendar-name-toggler_is_active")
	$(this).parent().children('.calendar__names').toggleClass('calendar__names_is_active');
});